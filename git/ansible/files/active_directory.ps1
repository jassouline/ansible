#Import ServerManager Module to install ADDS Binaries
Import-Module Servermanager

Import-Module ADDSDeployment

#We are installing the first domain controller in forest
$file = "c:\pw.txt"
#$pw | ConvertFrom-SecureString | Set-Content $file

Install-ADDSForest -CreateDNSDelegation:$false -DatabasePath "C:\Windows\NTDS" -DomainMode "Win2012R2" -DomainName "network-sdn.lan" -DomainNetbiosName "network-sdn" -safemodeadministratorpassword (ConvertTo-SecureString -String "Tarzan.4" -AsPlainText -Force) -ForestMode "Win2012R2" -InstallDns:$true -LogPath "C:\Windows\NTDS" -NoRebootOnCompletion:$false -SysvolPath "C:\Windows\SYSVOL" -Force:$true
